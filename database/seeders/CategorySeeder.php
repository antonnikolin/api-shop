<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories =[
            'Laptops and computers',
            'Smartphones, TV and electronics',
            'Goods for gamers',
            'Appliances',
            'Household products',
            'Tools and auto products',
            'Plumbing and repair',
            'Cottage, garden and vegetable garden',
            'Sports and hobbies',
            'Clothes, shoes and jewelry',
            'beauty and health',
            'Children\'s products',
            'Pet supplies',
        ];
        $data = [];
        for ($i = 0; $i < count($categories); $i++) {
            $data[] = [
                'name' => $categories[$i],
            ];
        }

        foreach ($data as $category) {
            Category::create($category);
        }

    }
}
