<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Admin',
            'surname' => 'Administrator',
            'email' => 'admin@gmail.com',
            'role' => 1,
            'email_verified_at' => now()->toDateTimeString(),
            'password' => Hash::make('12345678')
        ]);
        DB::table('users')->insert([
            'first_name' => 'Bill',
            'surname' => 'Gates',
            'email' => 'gates@gmail.com',
            'role' => 2,
            'email_verified_at' => now()->toDateTimeString(),
            'password' => Hash::make('12345678')
        ]);
    }
}
