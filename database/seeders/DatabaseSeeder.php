<?php

namespace Database\Seeders;

use App\Models\Cart;
use App\Models\Order;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       // User::factory(10)->create();
        $this->call([
            AdminSeeder::class,
            CategorySeeder::class,
            //CartSeeder::class,
        ]);
        Product::factory(30)->create();
        Cart::factory(12)
            ->hasAttached(Product::factory(3))
            ->create();
        Order::factory(15)->create();

    }
}
