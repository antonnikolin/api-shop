<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Artisan;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        Artisan::call('storage:link');
        return [
            'category_id' => rand(1,13),
            'name' => $this->faker->sentence(3),
            'excerpt' => $this->faker->text,
            'description' => $this->faker->realText(rand(500,1000)),
            'image_path' => $this->faker->image('public/storage',640,480, null, true),
            'count' => rand(0,10),
            'price' => $this->faker->randomFloat(2,500,2500),
        ];
    }
}
