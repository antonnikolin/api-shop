<?php

namespace App\Http\Middleware;

use App\Models\Enums\Role;
use Closure;
use Illuminate\Http\Request;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth('sanctum')->user()->role === Role::ADMINISTRATOR) {
            return $next($request);
        }
        return response()->json(['error' => 'No role admin']);
    }
}
