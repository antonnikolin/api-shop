<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'category_id' => ['integer', 'exists:categories,id', 'nullable'],
            'name' => ['string', 'nullable'],
            'excerpt' => ['string', 'nullable'],
            'description' => ['string', 'nullable'],
            'count' => ['integer', 'nullable'],
            'image' => ['file','mimes:jpeg,jpg,png','nullable','max:20480'],
            'price' => ['numeric', 'nullable'],
        ];
    }
}
