<?php

namespace App\Http\Requests;

use App\Models\Cart;
use App\Rules\PhoneNumber;
use Illuminate\Foundation\Http\FormRequest;

class OrderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'user_id' => ['integer', 'exists:users,id', 'required'],
            'cart_id' => ['integer', 'exists:carts,id', 'required'],
            'first_name' => ['string','required'],
            'surname' => ['string', 'required'],
            'phone' => [new PhoneNumber(), 'required'],
            'email' => ['email', 'required'],
            'address' => ['string', 'required'],
        ];
    }

    /**
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'user_id' => auth()->user()->id,
            'cart_id' => auth()->user()->cart->id
        ]);
    }
}
