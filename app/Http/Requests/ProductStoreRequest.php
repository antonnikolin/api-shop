<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'category_id' => ['integer', 'exists:categories,id', 'required'],
            'name' => ['string', 'required'],
            'excerpt' => ['string', 'required'],
            'description' => ['string', 'required'],
            'count' => ['integer', 'required'],
            'image' => ['file','mimes:jpeg,jpg,png','required','max:102400'],
            'price' => ['numeric', 'required'],
        ];
    }
}
