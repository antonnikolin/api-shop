<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReviewStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'product_id' => ['integer','exists:products,id', 'required'],
            'user_id' => ['integer', 'exists:users,id', 'required'],
            'rating' => ['integer', 'between:1,10', 'required'],
            'comment' => ['string', 'required']

        ];
    }

    /**
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'user_id' => auth('sanctum')->user()->id,
        ]);
    }
}
