<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderStoreRequest;
use App\Http\Requests\OrderUpdateRequest;
use App\Http\Resources\OrderResource;
use App\Models\Enums\Role;
use App\Models\Order;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        if (auth('sanctum')->user()->role === Role::ADMINISTRATOR) {

            return OrderResource::collection(Order::with('cart','cart.products', 'cart.user')
                ->paginate(request()->get('per_page', 10)));

        } elseif (auth('sanctum')->user()->role === Role::CUSTOMER) {

            return OrderResource::collection(auth()->user()->orders
                ->loadMissing('cart', 'cart.products'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return OrderResource
     */
    public function store(OrderStoreRequest $request)
    {
        return OrderResource::make(Order::create($request->validated())
            ->loadMissing('cart','cart.products'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return OrderResource
     */
    public function show(Order $order)
    {
        return OrderResource::make($order->loadMissing('cart', 'cart.products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return OrderResource
     */
    public function update(OrderUpdateRequest $request, Order $order)
    {
        $order->update($request->validated());

        return OrderResource::make($order->refresh()->loadMissing('cart', 'cart.products'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Order $order)
    {
        $order->delete();

        return response()->json(['message' => 'Ok']);
    }

    /**
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function processedStatus(Order $order)
    {
        $order->update([
            'status' => 'processed'
        ]);
        $order->cart()->delete();
        return response()->json(['message' => 'Ok']);
    }

}
