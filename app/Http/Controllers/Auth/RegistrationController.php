<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegistrationRequest;
use App\Http\Resources\UserResource;
use App\Models\Enums\Role;
use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;

class RegistrationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return UserResource
     */
    public function registration(RegistrationRequest $request)
    {
       $user = User::create([
           'first_name' => $request->validated('first_name'),
           'surname' => $request->validated('surname'),
           'email' => $request->validated('email'),
           'password' => Hash::make($request->validated('password')),
           'role' => Role::CUSTOMER,

       ]);

        event(new Registered($user));

       return UserResource::make($user);
    }

    /**
     * @param $id
     * @param $hash
     * @return \Illuminate\Http\JsonResponse
     */
    public function emailVerify($id, $hash)
    {

        $user = User::find($id);

        abort_if(!$user, 403);
        abort_if(!hash_equals($hash, sha1($user->getEmailForVerification())), 403);

        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
            event(new Verified($user));
        }

        return response()->json(['message' => 'Email verified'], 200);
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendEmailVerify(User $user)
    {
        $user->sendEmailVerificationNotification();

        return response()->json(['message' => 'Confirmation email resent']);
    }
}
