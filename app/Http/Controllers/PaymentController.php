<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentRequest;
use App\Models\Product;
use Illuminate\Http\Request;
use Laravel\Cashier\Cashier;
use Stripe\Customer;
use Stripe\Stripe;


class PaymentController extends Controller
{
    /**
     * @param PaymentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function payment(PaymentRequest $request)
    {
        $user = auth('sanctum')->user();
        $data = $request->validated();
        $amount = 0;

        foreach ($data['products'] as $product) {
            $product_count = $user->cart->products()
                ->where('product_id', $product['id'])->first()->pivot->count;

            if ($product['count'] !== $product_count){
                return response()->json(['error' => 'Quantity of goods is not correct']);
            }
            $prod = Product::where('id', $product['id'])->first();
            $amount += ($prod->price * $product['count']);
        }

        $user->createOrGetStripeCustomer([
            'name' => $user->name.' '.$user->surname,
        ]);

        $user->createSetupIntent();

        $user->cart->order()->update(['status' => 'sent']);
        $stripeCharge = $user->charge($amount, $data['payment_method_id']);

         return $stripeCharge;
    }


}
