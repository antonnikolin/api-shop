<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartRequest;
use App\Http\Resources\CartResource;
use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return CartResource
     */
    public function addProduct(CartRequest $request)
    {
        $data = $request->validated();
        list($cart, $product) = $this->getCartProduct($data);

        if(is_null($cart)) {
            $data['total_amount'] = $product->price * $data['count'];
            $data['user_id'] = auth()->user()->id;
            $cart = Cart::create($data);

            $cart->products()->attach($data['product_id'],['count' => $data['count']]);
        } else {
            $cart->total_amount +=  $product->price * $data['count'];
            $cart->update([
                'total_amount' => $cart->total_amount,
            ]);

            if ($cart->products->contains($product->id)) {
                $pivot_count = $cart->products()->where('product_id', $product->id)->first()->pivot->count;
                $cart->products()->updateExistingPivot($data['product_id'], ['count' => $pivot_count + $data['count']]);

            } else {
                $cart->products()->attach($data['product_id'],['count' => $data['count']]);
            }
        }
        $product->update(['count' => ($product->count - $data['count'])]);

        return CartResource::make($cart->refresh()->loadMissing('user', 'products'));
    }

    /**
     * Display the specified resource.
     *
     * @return CartResource
     */
    public function show()
    {
        return CartResource::make(auth()->user()->cart->loadMissing('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return CartResource
     */
    public function removeProduct(CartRequest $request)
    {
        $data = $request->validated();
        list($cart, $product) = $this->getCartProduct($data);
        $pivot_count = $cart->products()->where('product_id', $product->id)->first()->pivot->count;
        $pivot_count -= $data['count'];
        if ($pivot_count === 0)
        {
            $cart->products()->detach($data['product_id']);
        } else {
            $cart->products()->updateExistingPivot($data['product_id'], ['count' => $pivot_count]);
        }
            $cart->total_amount -=  $product->price * $data['count'];
            $cart->update([
                'total_amount' => $cart->total_amount,
            ]);
            $product->update(['count' => ($product->count + $data['count'])]);
        return CartResource::make($cart->refresh()->loadMissing('user','products'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Cart $cart)
    {
        $cart->products()->detach();
        $cart->forceDelete();

        return response()->json(['message' => 'Ok'], 200);
    }

    /**
     * @param CartRequest|Request $request
     * @return array
     */
    public function getCartProduct($data): array
    {
        $cart = auth()->user()->cart;
        $product = Product::find($data['product_id']);

        return array($cart,$product);
    }

}
