<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return ProductResource::collection(Product::filter()
            ->paginate(request()->get('per_page', 10)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ProductResource
     */
    public function store(ProductStoreRequest $request)
    {
        $data = $request->validated();
        $data['image_path'] = $request->file('image')->store('public/images');
        unset($data['image']);

        return ProductResource::make(Product::create($data)->loadMissing('category'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return ProductResource
     */
    public function show(Product $product)
    {
        $product->image_path = request()->getSchemeAndHttpHost().Storage::url($product->image_path);
        return ProductResource::make($product->loadMissing('category','reviews', 'reviews.user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return ProductResource
     */
    public function update(ProductUpdateRequest $request, Product $product)
    {
        $data = $request->validated();
        if ($request->file('image'))
        {
            Storage::delete($product->image_path);
            $data['image_path'] = $request->file('image')->store('public/images');
            unset($data['image']);
        }
        $product->update($data);

        return ProductResource::make($product->refresh()->loadMissing('category'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Product $product)
    {
        Storage::delete($product->image_path);
        $product->delete();
        return response()->json(['message' => 'Ok'], 200);
    }
}
