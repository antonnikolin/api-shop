<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReviewStoreRequest;
use App\Http\Requests\ReviewUpdateRequest;
use App\Http\Resources\ReviewResource;
use App\Models\Review;

class ReviewController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return ReviewResource
     */
    public function store(ReviewStoreRequest $request)
    {
        return ReviewResource::make(Review::create($request->validated()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return ReviewResource
     */
    public function update(ReviewUpdateRequest $request, Review $review)
    {
        if ($review->user_id === auth()->user()->id)
        {
            $review->update($request->validated());

            return ReviewResource::make($review);
        } else {
            return response()->json(['message' => 'You are not allowed to edit this comment']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Review $review)
    {
        $review->delete();

        return response()->json(['message' => 'Ok']);
    }
}
