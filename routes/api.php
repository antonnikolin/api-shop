<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegistrationController;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\ChangePasswordController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ReviewController;
use App\Http\Controllers\PaymentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function (){

    Route::post('/login',[LoginController::class, 'login']);
    Route::post('/logout',[LoginController::class, 'logout']);
    Route::post('/registration', [RegistrationController::class, 'registration']);
    Route::get('/registration/email-verify/{id}/{hash}', [RegistrationController::class, 'emailVerify'])
        ->name('verification.verify');
    Route::post('/forgot-password', ForgotPasswordController::class);
    Route::post('/reset-password', ResetPasswordController::class);
    Route::get('/products', [ProductController::class, 'index']);
    Route::get('/products/{product}', [ProductController::class, 'show']);

    Route::middleware('auth:sanctum')->group(function () {
        Route::post('/registration/resend-verify/{user}', [RegistrationController::class, 'resendEmailVerify']);
        Route::post('/change-password', ChangePasswordController::class);
        Route::get('/orders', [OrderController::class, 'index']);

        Route::middleware('isAdmin')->group(function () {
            Route::apiResource('/users', UserController::class);
            Route::apiResource('/categories', CategoryController::class);
            Route::post('/products', [ProductController::class, 'store']);
            Route::post('/products/{product}', [ProductController::class, 'update']);
            Route::delete('/products/{product}', [ProductController::class, 'destroy']);
            Route::post('/orders/{order}/processed', [OrderController::class, 'processedStatus']);
            Route::delete('/reviews/{review}', [ReviewController::class, 'destroy']);

        });

        Route::middleware('isCustomer')->group(function () {
            //Route::apiResource('carts', CartController::class);
            Route::post('/carts/add', [CartController::class, 'addProduct']);
            Route::post('/carts/remove', [CartController::class, 'removeProduct']);
            Route::get('/carts', [CartController::class, 'show']);
            Route::delete('/carts/{cart}', [CartController::class, 'destroy']);
            Route::apiResource('/orders', OrderController::class)->except('index');
            Route::post('/reviews', [ReviewController::class, 'store']);
            Route::put('/reviews/{review}', [ReviewController::class, 'update']);
            Route::post('/payments', [PaymentController::class, 'payment']);
        });


    });
});
